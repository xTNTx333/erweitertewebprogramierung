<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Felix Bruns's Mallspass</title>
</head>
<body>
<h1>Hallo und Willkommen zum Bunten Malspass</h1>
<form action="rest/paint/image" method="post" id="mainform">
<label for="nicname">NicName</label>
<input type="text" id="nicname" />
<br />
<label for="channel">Kanal</label><br />
<input name="channel" id="Radio1" value="1" type="radio" checked="checked">Kanal 1</input><br />
<input name="channel" id="Radio2" value="2" type="radio">Kanal 2</input><br />
<br />
<button type="submit">Bild Anzeigen</button>
<button type="button" onclick="NavigateToMultiPaint()">Jetzt Zeichnen</button>
<button type="button" onclick="NavigateToJSON()">JSON</button>
</form>
<script type="text/javascript">
function NavigateToJSON(){
	var channels = document.getElementsByName("channel");
	var channel = null;
	for(index = 0; index < channels.length; ++index){
		if(channels[index].checked){
			channel = channels[index];
			break;
		}
	}
	var escapeChannel = encodeURI(channel.value);
	var temp = window.location.href.replace('index.jsp', 'rest/paint/json/' +  escapeChannel); 
	window.location = temp;
}
function NavigateToMultiPaint(){
	var nicname = document.getElementById('nicname');
	var channels = document.getElementsByName("channel");
	var channel = null;
	for(index = 0; index < channels.length; ++index){
		if(channels[index].checked){
			channel = channels[index];
			break;
		}
	}
	if(nicname != null && nicname.value != ''){
		var escapename = encodeURI(nicname.value);
		var escapeChannel = encodeURI(channel.value)
		var temp = window.location.href.replace('index.jsp', 'rest/paint/multypaint/' +  escapeChannel + '/' + escapename); 
		window.location = temp;
	}else{
		alert("NicName darf nicht leer sein");
	}
}
</script>
</body>
</html>