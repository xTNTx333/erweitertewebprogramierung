var canvas;
var cntxt;
var canvastop;
var left;
var brush;
var color;
var historyArray;
var currentHistory;
var fill;
var listHistory;
var ws;
var room;
var name;
var textbox;
var chatMessages;
var filter;
//Inizieren aller wichtigen Variablen und Funktionen
function initialise() {
	//Alle wichtigen Elemente raus suchen
	brush = document.getElementById("brush");
	color = document.getElementById('html5colorpicker');
	fill = document.getElementById('fill');
	textbox = document.getElementById('newMessage');
	chatMessages = document.getElementById('chatMessage');
	filter = document.getElementById('filter');
	//Informationen ermitteln unter welchen Raum und Name das Script laufen soll
	var meta = document.getElementsByTagName('meta');
	var index;
	for (index = 0; index < meta.length; ++index) {
		if (meta[index].name == 'room') {
			room = meta[index].content;
		} else if (meta[index].name == 'nickname') {
			name = meta[index].content;
		}
	}
	//Die Burst Inizieren
	brushChange();
	//Canvas beleben
	canvas = document.getElementById("canvas_1");
	listHistory = document.getElementById('history');
	canvas.style.cursor = 'crosshair';
	canvas.addEventListener("mousedown", doMouseDown, false);
	canvas.addEventListener("mouseup", doMouseUp, false);
	cntxt = canvas.getContext("2d");
	cntxt.strokeStyle = color.value;
	cntxt.lineWidth = 5;
	cntxt.lineCap = 'round';
	rect = canvas.getBoundingClientRect();
	canvastop = rect.top;
	left = rect.left;
	historyArray = new Array();
	//Fals ein user auf die Idee kommt das Fenster neu zu laden Schliessen wir den Socket
	window.onbeforeunload = function() {
		if (ws != null && ws.readyState < 2) {
			ws.close();
		}
	}
	//Offnen des Websockets
	ws = new WebSocket('ws:' + location.hostname + '/wp_sose15_01/' + 'websocked/' + room + '/'
			+ name);
	//Beim offnen verschicken wir die Nachricht das wir alles Neu emfangen wollen
	ws.onopen = function() {
		var msg = new message('AllContent');
		ws.send(JSON.stringify(msg));
	}
	//Behandlung bei eingehenden Nachrichten
	ws.onmessage = function() {
		try {
			//Daten auspaken
			var data = arguments[0].data;
			var obj = JSON.parse(data);
			//Wenn es sich um eine neuen eintrag handelt
			if (obj.type == 'NewContent') {
				var newHistory = JSON.parse(obj.data);
				//Fuer alle die Eintrage die noch nicht hinzugefuegt worden sind Zeichnen wir sie und 
				//Fuegen sie hinzu
				for (var i = historyArray.length; i < newHistory.length; ++i) {
					var currentHisory = new historyEntry(newHistory[i]);
					historyArray.push(currentHisory);
					doPaint(currentHisory);
					addHistoryEntry(currentHisory);
				}
			} else if (obj.type == 'Reset') {
				var newHistory = JSON.parse(obj.data);
				//History eintrage aus der Liste entfernen
				var childes = listHistory.childNodes;
				for(var i = childes.length -1; i >= 0 ; --i){
					listHistory.removeChild(childes[i]);
				}
				//Canvas loschen
				cntxt.clearRect(0, 0, canvas.width, canvas.height);
				historyArray = new Array();
				//Alle Elemente jetzt wieder hinzufuegen und Zeichnen
				for (var i = historyArray.length; i < newHistory.length; ++i) {
					var currentHisory = new historyEntry(newHistory[i]);
					historyArray.push(currentHisory);
					doPaint(currentHisory);
					addHistoryEntry(currentHisory);
				}
			} else if(obj.type == 'TextMassage'){
				//Bei einer Textnachricht einfach den Text ausgeben
				var listElement = document.createElement('li');
				var msg = new chatmessage(JSON.parse(obj.data));
				listElement.innerHTML = msg.user + ": \"" + msg.message + "\"";
				chatMessages.appendChild(listElement);
			}
		} catch (ex) {
			console.warn(ex);
		}
	}
	//Beim Schliessen des Sockets wieder neue verbindung aufbauen
	ws.onclose = function() {
		ws = new WebSocket('ws:' + location.hostname + '/wp_sose15_01/' + 'websocked/' + room + '/'
				+ name);
	}
}
//Funktion liest die den gewahlten Zeichenart aus
function brushChange() {
	var checkBox = document.getElementById('fill');
	if (brush.value == 'line') {
		checkBox.checked = false;
		checkBox.setAttribute('disabled', 'disabled');
	} else {
		checkBox.removeAttribute('disabled');
	}
}
//Beinhaltet eine Zeichenoperation
function historyEntry() {
	this.user = name;
	this.PointX = 0;
	this.PointY = 0;
	this.PointX2 = 0;
	this.PointY2 = 0;
	this.Id = 0;
	this.drawType = brush.value;
	this.color = color.value;
	this.isFill = fill.checked;
	//Dient dazu aus einen JSON String wieder ein HistoryEntry zu erzeugen
	if (historyEntry.arguments.length > 0) {
		try {
			for ( var prop in historyEntry.arguments[0]) {
				this[prop] = historyEntry.arguments[0][prop];
			}
		} catch (e) {
			console.warn("Erstes argument war kein HistoryEntry" + e);
		}
	}
	//Zur Darstellung des Objektes
	this.GetString = function() {
		var gr;
		switch (this.drawType) {
		case 'line':
			gr = 'eine Linie';
			break;
		case 'sqare':
			gr = 'ein Quardrat';
			break;
		default:
			gr = 'einene Kreis';
		}
		return this.user + " Zeichnet " + gr + " in der Farbe " + this.color;
	}
}
//Dient zur Kommunication zwischen Client und Server
function message(type, data) {
	this.type = type;
	this.data = JSON.stringify(data);
}
//Beim Click auf die Canvas Flache erzeugen wir einen neuen History eintrag und setzen die Werte
function doMouseDown(event) {
	currentHistory = new historyEntry();
	currentHistory.PointX = event.clientX - left;
	currentHistory.PointY = event.clientY - canvastop;
	x = event.clientX;
	y = event.layerY;
	document.getElementById("value_x").innerHTML = x;
	document.getElementById("value_y").innerHTML = y;
}
//Wenn die maus gehoben wird holen wir uns die Position der maus und senden den Historyeintrag an
//den Server.
function doMouseUp(event) {
	x = event.clientX;
	y = event.clientY;
	document.getElementById("value_x").innerHTML = x + "/" + left;
	document.getElementById("value_y").innerHTML = y + "/" + canvastop;
	currentHistory.PointX2 = x - left + 1;
	currentHistory.PointY2 = y - canvastop + 1;
	var msg = new message('NewContent', currentHistory);
	var json = JSON.stringify(msg);
	try{
	ws.send(json);
	}catch (e) {
		window.alert("Fehler beim Senden" + e);
	}
}
//Fuegt der History auflistung ein neuen eintrag zu
function addHistoryEntry(hEntry) {
	var listElement = document.createElement('li');
	listElement.innerHTML = hEntry.GetString();
	var button = document.createElement('button');
	button.innerHTML = 'Loeschen';
	button.setAttribute ('onClick', 'DeleteHistoryEntry(' + hEntry.Id + ');');
	listElement.appendChild(button);
	listHistory.appendChild(listElement);
}
function DeleteHistoryEntry(ID){
	var msg = new message('Delete', ID);
	ws.send(JSON.stringify(msg));
}
//Die Funktion zeichnet auf die Canvas Flache
function doPaint(hEntry) {
	//Farbe setzen
	cntxt.strokeStyle = hEntry.color;
	cntxt.fillStyle = hEntry.color;
	//Linienstarke setzen
	cntxt.lineWidth = 10;
	cntxt.beginPath();
	if (hEntry.drawType == 'line') {
		cntxt.moveTo(hEntry.PointX, hEntry.PointY);
		cntxt.lineTo(hEntry.PointX2, hEntry.PointY2);
	} else if (hEntry.drawType == 'sqare') {
		cntxt.rect(hEntry.PointX, hEntry.PointY, -1
				* (hEntry.PointX - hEntry.PointX2), -1
				* (hEntry.PointY - hEntry.PointY2));
	} else if (hEntry.drawType == 'cical') {
		var distance = Math.sqrt(Math.pow(hEntry.PointX - hEntry.PointX2, 2)
				+ Math.pow(hEntry.PointY - hEntry.PointY2, 2));
		cntxt.arc(hEntry.PointX, hEntry.PointY, distance, 0, 2 * Math.PI);
	}	
	//Line ist hier ausgenommen damit nicht bei Falschen einstellungen nichts anzeigt
	if (hEntry.isFill && hEntry.drawType != 'line') {
		cntxt.fill();
	} else {
		cntxt.stroke();
	}
}
//Klasse fuer Chatmassages
function chatmessage(){
	this.user = name;
	this.message = textbox.value;
	if (chatmessage.arguments.length > 0) {
		try {
			for ( var prop in chatmessage.arguments[0]) {
				this[prop] = chatmessage.arguments[0][prop];
			}
		} catch (e) {
			console.warn("Erstes argument war kein user" + e);
		}
	}
}
//Sendet eine Text Nachricht
function sendChatMessage(){
	var msg = new message('TextMassage', new chatmessage());
	ws.send(JSON.stringify(msg));
	return false;
}
//Wird ausgefuert wenn ein Filter abgesendt wird
function executeFilter(){
	var msg = new message('Filter', filter.options[filter.selectedIndex].value);
	ws.send(JSON.stringify(msg));
}