<!DOCTYPE HTML>
<html>
<head>
<title>Lets Paint</title>
<meta name="room" content="${it.room }" />
<meta name="nickname" content="${it.nickname }" />
<script type="text/javascript" src="/wp_sose15_01/drawcanvas.js">

<!-- required for FF3 and Opera -->
	
</script>
</head>
<body onload="initialise()">
	<div id="drawdiv" style="display: inline-flex;">
		<div id="brushdiv" style="border-color: black; border-style: solid;">
			<label for="brush">Pinsel</label> <select id="brush"
				onchange='brushChange()'>
				<option value="line">Line</option>
				<option value="sqare">Quardrat</option>
				<option value="cical">Kreis</option>
			</select> <br /> <label for="color">Farbe</label> <input type="color"
				id="html5colorpicker" value="#ff0000" /> <br /> <label for='fill'>Fullen</label>
			<input id='fill' type='checkbox' />
		</div>
		<div id="paintdiv" style="border-color: black; border-style: solid;">
			<canvas width="500" height="500" id="canvas_1">

</canvas>
		</div>

		<div id="historydiv" style="border-color: black; border-style: solid;">
			<label for="filter">Filter: </label> <select id="filter">
				<option value="line">Linien</option>
				<option value="sqare">Quadrate</option>
				<option value="cical">Kreis</option>
				<option value="robot">Robot</option>
				<option selected="selected" value="none">Kein Filter</option>
			</select>
			<button onclick="executeFilter()">Filter Anwenden</button>
			<div id='historylist' style='height: 300px; overflow: scroll;'>
				<h5>History</h5>
				<br />
				<ul id='history'>

				</ul>
			</div>
			Value x: <span id="value_x"></span> <br> Value y: <span
				id="value_y"></span>
		</div>
	</div>
    <div id="chat" style='height: 100px; min-width: 300px; overflow: scroll;'>
    <ul id="chatMessage">
    </ul>
    </div>
    <br />
    <div id='chatMessageDiv'>
    <form action="#" onsubmit="sendChatMessage()">
    <input type="text" id="newMessage" autocomplete="off" style="min-width: 600px"/>
    <button type="submit">Senden</button>
    </form>
    </div>
</body>
</html>