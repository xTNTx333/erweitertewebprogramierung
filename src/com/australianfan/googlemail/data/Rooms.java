package com.australianfan.googlemail.data;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.australianfan.googlemail.controler.GrafikSocket;
import com.australianfan.googlemail.controler.Robot;

/**
 * Die einzelnen Raeume werden hier verwaltet und dies ist als Singelten
 * ausgelegt
 * 
 * @author Felix Bruns
 *
 */
public class Rooms {
	/**
	 * Privater Konstruktor damit nicht versehendlicht diese Klasse erstelt wird
	 */
	private Rooms() {

	}

	/**
	 * Instanz die verwendet wird
	 */
	private static Rooms instance = null;

	/**
	 * Map mit der Zuornung von Raum und History
	 */
	private Map<String, List<HistoryEntry>> roomHistory = new HashMap<String, List<HistoryEntry>>();

	/**
	 * Map mit allen Sockets die in eien Raum exsitieren
	 */
	private Map<String, List<GrafikSocket>> roomSocket = new HashMap<String, List<GrafikSocket>>();

	/**
	 * Map mit Robotern fuer eien raum
	 */
	private Map<String, Robot> roomRobot = new HashMap<String, Robot>();

	/**
	 * Fuegt einen Raum einen History eintrag hinzu
	 * 
	 * @param room
	 *            Der Raum
	 * @param entry
	 *            Der Eintrag der hinzugefuegt werden soll
	 */
	public void AddHistoryEntry(String room, HistoryEntry entry) {
		if (!roomHistory.containsKey(room)) {
			roomHistory.put(room, new LinkedList<HistoryEntry>());
		}

		List<HistoryEntry> entries = roomHistory.get(room);
		entry.setId(entries.size());
		entries.add(entry);

		// Alle Clients im Raum benachrichtigen
		for (GrafikSocket socket : roomSocket.get(room)) {
			socket.sendHistory(entries, false);
		}
	}

	/**
	 * Gibt die History zu einen Raum raus
	 * 
	 * @param room
	 *            Der Raum
	 * @return List mit den Historyeintraegen
	 */
	public List<HistoryEntry> GetHistory(String room) {
		if (!roomHistory.containsKey(room)) {
			roomHistory.put(room, new LinkedList<HistoryEntry>());
		}

		return roomHistory.get(room);
	}

	/**
	 * Fuegt einen neuen GrafikSoket den Raum hinzu
	 * 
	 * @param room
	 *            Der Raum
	 * @param socket
	 *            Der Neue Socket
	 */
	public void RegisterGrafikSoket(String room, GrafikSocket socket) {
		if (!roomSocket.containsKey(room)) {
			roomSocket.put(room, new LinkedList<GrafikSocket>());
		}

		List<GrafikSocket> sockets = roomSocket.get(room);
		sockets.add(socket);

		// Aktivieren oder Deaktivieren des Robots
		if (sockets.size() < 4 && !roomRobot.containsKey(room) && !sockets.isEmpty()) {
			Robot robot = new Robot(room);
			robot.Start();
			roomRobot.put(room, robot);
		} else if ((sockets.isEmpty() || sockets.size() >= 4) && roomRobot.containsKey(room)) {
			roomRobot.get(room).Stop();
			roomRobot.remove(room);
		}
	}

	/**
	 * Entfernt aus dem Raum einen GrafikSoket
	 * 
	 * @param room
	 *            Der Raum
	 * @param socket
	 *            Der Socket
	 */
	public void UnRegisterGrafikSocket(String room, GrafikSocket socket) {
		if (roomSocket.containsKey(room)) {
			List<GrafikSocket> sockets = roomSocket.get(room);
			sockets.remove(socket);

			// Aktivieren oder Deaktivieren des Robots
			if ((!sockets.isEmpty() || sockets.size() >= 4) && !roomRobot.containsKey(room)) {
				Robot robot = new Robot(room);
				robot.Start();
				roomRobot.put(room, robot);
			} else if (sockets.isEmpty() && roomRobot.containsKey(room)) {
				roomRobot.get(room).Stop();
				roomRobot.remove(room);
			}
		}
	}

	/**
	 * Sendet eine Text Nachrichte an alle Clients in einen Raum
	 * 
	 * @param room
	 *            Der Raum
	 * @param user
	 *            Der User der die Nachricht Senden will
	 * @param message
	 *            Die Nachricht
	 */
	public void TextMessage(String room, String user, ChatData message) {
		if (roomSocket.containsKey(room)) {
			Message msg = new Message(MessageType.TextMassage, message);
			for (GrafikSocket socket : roomSocket.get(room)) {
				socket.sendText(msg);
			}
		}
	}

	/**
	 * Fuert einen Filter durch
	 * 
	 * @param room
	 *            Der Raum
	 * @param Filter
	 *            Der zu verwendene Filter
	 */
	public void executeFilter(String room, String Filter) {
		if (roomHistory.containsKey(room)) {
			List<HistoryEntry> entriesNew = new LinkedList<HistoryEntry>();
			for (HistoryEntry entry : roomHistory.get(room)) {
				if (Filter.equals("robot")) {
					if (entry.getUser() != "Robot") {
						entriesNew.add(entry);
					}
				} else {
					if (!entry.getDrawType().equals(Filter)) {
						entriesNew.add(entry);
					}
				}
			}

			roomHistory.put(room, entriesNew);

			for (GrafikSocket socket : roomSocket.get(room)) {
				socket.sendHistory(entriesNew, true);
			}
		}
	}

	/**
	 * Entfernt einen eintrag aus der History
	 * 
	 * @param room
	 *            Der raum fuer die History
	 * @param id
	 *            Die ID die entfernt werden soll
	 */
	public void delteHistoryEntry(String room, int id) {
		if (roomHistory.containsKey(room)) {
			List<HistoryEntry> entriesNew = new LinkedList<HistoryEntry>();
			for (HistoryEntry entry : roomHistory.get(room)) {
				if (entry.getId() != id) {
					entriesNew.add(entry);
				}
			}

			roomHistory.put(room, entriesNew);

			for (GrafikSocket socket : roomSocket.get(room)) {
				socket.sendHistory(entriesNew, true);
			}
		}
	}

	/**
	 * Instanz der Klasse
	 * 
	 * @return Das Singelton
	 */
	public static Rooms getIstance() {
		if (instance == null) {
			instance = new Rooms();
		}

		return instance;
	}
}
