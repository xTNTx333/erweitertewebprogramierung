package com.australianfan.googlemail.data;

import java.util.logging.Level;

import com.google.gson.Gson;
import com.sun.istack.internal.logging.Logger;

/**
 * Klasse dient zum Austausch von Informationen zwischen Client und Server
 * @author Felix Bruns
 *
 */
public class Message {
	/**
	 * Standart Konstructor
	 */
	public Message(){}
	
	/**
	 * Konstruktor mit den Wichtigen Parametern
	 * @param type Art der Nachricht
	 * @param data Daten die die Nachricht enthalten soll
	 */
	public Message(MessageType type, Object data){
		this.type = type;
		Gson gson = new Gson();
		this.data = gson.toJson(data);
	}
	
	/**
	 * Logger zum loggen von Informationen
	 */
	private static Logger log = Logger.getLogger(Message.class);
	
	/**
	 * Art der Nachricht
	 */
	private MessageType type;
	
	/**
	 * Daten der Nachricht
	 */
	private String data;
	
	/**
	 * Gibt den Typ der Nachricht zurueck
	 * @return Nachricht Typ
	 */
	public MessageType getTyp(){
		if(type != null){
			return type;
		}
		
		return null;
	}
	
	/**
	 * Liefert die Daten zuruek
	 * @return Die Daten
	 */
	public Object getData(){
		Object temp = null;
		Gson gson = new Gson();
		try{
		switch (type) {
		case NewContent:
			temp = gson.fromJson(data, HistoryEntry.class);
			break;
		case TextMassage:
			temp = gson.fromJson(data, ChatData.class);
			break;
		case Filter:
			temp = gson.fromJson(data, String.class);
			break;
		case Delete:
			temp = gson.fromJson(data, Integer.class);
			break;
		default:
			temp = null;
			break;
		}
		}catch(Exception ex){
			log.log(Level.WARNING, "Fehler beim Enpacken der Nachricht", ex);
		}
		return temp;
	}

	/**
	 * Gibt den Typ der Nachricht zurueck
	 * @param type Typ der Nachricht
	 */
	public void setType(String type){
		if(type != null){
			this.type = MessageType.valueOf(type);
		}
	}
	
	/**
	 * Setzt den Typ der Nachricht
	 * @param type Typ der Nachricht
	 */
	public void setType(MessageType type) {
		this.type = type;
	}

	/**
	 * Setzt die Daten (Daten mussen im JSON format gesetzt werden)
	 * @param data neue Daten
	 */
	public void setData(String data) {
		this.data = data;
	}
}
