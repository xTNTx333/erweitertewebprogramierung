package com.australianfan.googlemail.data;
/**
 * Enum zum Entscheiden um was es sich fuer eine Nachricht handelt
 * @author Felix Bruns
 *
 */
public enum MessageType {
	Reset,
	AllContent,
	NewContent,
	TextMassage,
	HistoryEntry,
	Filter,
	Close,
	Delete
}
