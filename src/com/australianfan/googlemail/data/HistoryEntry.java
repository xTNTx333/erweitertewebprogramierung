package com.australianfan.googlemail.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HistoryEntry {
	/**
	 * User der fuer die Nachricht verantwortlich ist
	 */
	private String user;
	
	/**
	 * X Kordinate wo Maus gedruckt wurde
	 */
	private Double PointX;
	
	/**
	 * Y Kordinate wo Maus gedruckt wurde
	 */
	private Double PointY;
	
	/**
	 * X Kordinate wo Maus losgelassen wurde
	 */
	private Double PointX2;
	
	/**
	 * Y Kordinate wo Maus losgelassen wurde
	 */
	private Double PointY2;
	
	/**
	 * Id des Eintrags
	 */
	private Integer Id;
	
	/**
	 * Was Gezeichnet werden soll
	 */
	private String drawType;
	
	/**
	 * In welcher Farbe gezeichnet werden soll
	 */
	private String color;
	
	/**
	 * Ob die Form ausgefult ist
	 */
	private Boolean isFill;
	
	/**
	 * Gibt den User Zurueck
	 * @return Der User
	 */
	public String getUser() {
		return user;
	}
	
	/**
	 * Setzt den User
	 * @param user Der User
	 */
	public void setUser(String user) {
		this.user = user;
	}
	
	/**
	 * Gibt die X Kordinate wieder wo die Maus gedruckt wurde
	 * @return X Kordinate
	 */
	public Double getPointX() {
		return PointX;
	}
	
	/**
	 * Setzt die X Kordinate wo die Maus gedruckt wurde
	 * @param pointX Neue X Kordinate
	 */
	public void setPointX(Double pointX) {
		PointX = pointX;
	}
	
	/**
	 * Gibt die Y Kordinate zurueck wo die Maus gedruckt wurde
	 * @return Y Kordinate
	 */
	public Double getPointY() {
		return PointY;
	}
	
	/**
	 * Setzt die Y Kordinate an der die Maus gedruckt wurde
	 * @param pointY Die neue Y Kordinate
	 */
	public void setPointY(Double pointY) {
		PointY = pointY;
	}
	
	/**
	 * Gibt die X Kordinate wieder wo die Maus losgelassen wurde
	 * @return X Kordinate
	 */
	public Double getPointX2() {
		return PointX2;
	}
	
	/**
	 * Setzt die X Kordinate wo die Maus losgelassen wurde
	 * @param pointX2 Die neue X Kordinate
	 */
	public void setPointX2(Double pointX2) {
		PointX2 = pointX2;
	}
	
	/**
	 * Gibt die Y Kordinate wo die Maus losgelassen wurde
	 * @return Y Kordinate
	 */
	public Double getPointY2() {
		return PointY2;
	}
	
	/**
	 * Setzt die Y Kordinate wo die Maus losgelassen wurde
	 * @param pointY2 Die neue Y Kodinate
	 */
	public void setPointY2(Double pointY2) {
		PointY2 = pointY2;
	}
	
	/**
	 * Gibt die ID zurueck
	 * @return ID
	 */
	public Integer getId() {
		return Id;
	}
	
	/**
	 * Setzt eine neue ID
	 * @param id Neue ID
	 */
	public void setId(Integer id) {
		Id = id;
	}
	
	/**
	 * Gibt zurueck was gezeichet werden soll
	 * @return Was gezeichnet werden soll
	 */
	public String getDrawType() {
		return drawType;
	}
	
	/**
	 * Setzt was gezeichnet werden soll
	 * @param drawType String mit den moglichen werten 'line', 'sqare' und 'cical'
	 */
	public void setDrawType(String drawType) {
		this.drawType = drawType;
	}
	
	/**
	 * Gibt die Farbe zurueck
	 * @return Farbe in Hex
	 */
	public String getColor() {
		return color;
	}
	
	/**
	 * Setzt die Farbe 
	 * @param color Hexwert der Farbe
	 */
	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Gibt an ob das zu zeichnen Objekt gefult ist
	 * @return True wenn gefullt sein soll
	 */
	public Boolean getIsFill() {
		return isFill;
	}
	
	/**
	 * Sezt den Wert ob das zu zeichnen Objekt gefult sein soll
	 * @param isFill True wenn gefullt
	 */
	public void setIsFill(Boolean isFill) {
		this.isFill = isFill;
	}
	
}
