package com.australianfan.googlemail.data;
/**
 * Diese Klasse Representiert eine Chatnachricht
 * @author Felix Bruns
 *
 */
public class ChatData {
	/**
	 * Standart Konstruktor
	 */
	public ChatData(){}
	
	/**
	 * Konstruktor der die Nachricht mit allen Werten fullt
	 * @param user Der User der die Nachricht gesendet hat
	 * @param message Der Text der Nachricht
	 */
	public ChatData(String user, String message){
		this.user = user;
		this.message = message;
	}
	
	/**
	 * User der die Nachricht geschrieben hat
	 */
	private String user;
	
	/**
	 * Die Nachricht selber
	 */
	private String message;
	
	/**
	 * Giebt den User der Nachricht zurueck
	 * @return Der User
	 */
	public synchronized String getUser() {
		return user;
	}

	/**
	 * Giebt die Nachricht zurueck
	 * @return Die Nachricht
	 */
	public synchronized String getMessage() {
		return message;
	}

	/**
	 * Setzt den User
	 * @param user Der User
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Setzt die Nachricht
	 * @param message Die Nachricht
	 */
	public void setMessage(String message) {
		this.message = message;
	}	
}
