package com.australianfan.googlemail.view;
/**
 * Klasse fuer die Notigen informationen der Malseite
 * @author Felix Bruns
 *
 */
public class SocketBean {
	/**
	 * Konstrucktor zum fullen der Bean
	 * @param room Der Raum fur die Seite
	 * @param nickname Der Gewahlte Name
	 */
	public SocketBean(String room, String nickname){
		this.room = room;
		this.nickname = nickname;
	}
	
	/**
	 * Der Raum fur die Seite
	 */
	private String room;
	
	/**
	 * Der Gewahlte Name
	 */
	private String nickname;
	
	/**
	 * Gibt den Raum Zurueck
	 * @return Der Raum
	 */
	public String getRoom() {
		return room;
	}
	
	/**
	 * Gibt den Namen zurueck
	 * @return Der Name
	 */
	public String getNickname() {
		return nickname;
	}
	
}
