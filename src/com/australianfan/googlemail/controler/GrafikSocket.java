package com.australianfan.googlemail.controler;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import com.australianfan.googlemail.data.ChatData;
import com.australianfan.googlemail.data.HistoryEntry;
import com.australianfan.googlemail.data.Message;
import com.australianfan.googlemail.data.MessageType;
import com.australianfan.googlemail.data.Rooms;
import com.google.gson.Gson;
/**
 * Klasse die einen Soket zur Kommunikation mit einen Webclienten uebernimmt
 * @author Felix Bruns
 *
 */
@ServerEndpoint("/websocked/{room}/{nicname}")
public class GrafikSocket {
	/**
	 * Die Session die verwendet wird
	 */
	private Session session;
	
	/**
	 * Der Raum in der die Session gultig ist
	 */
	private String room;
	
	/**
	 * Der Nicname des Benutzers
	 */
	private String nicname;
	
	/**
	 * Logger zum loggen von Informationen
	 */
	private static Logger log = Logger.getLogger(GrafikSocket.class.getName());
	
	/**
	 * Parameter der Ferhindern soll das Destroy mehrfach aufgerufen wird
	 */
	private boolean closing = false;
	
	/**
	 * Wird aufgerufen wenn eine Session aufgebaut wird
	 * @param session Die Session die aufgebaut wurde
	 * @param room Der Raum in dem die Session gultig ist
	 * @param nicname Der Nicname des Benutzers
	 */
	@OnOpen
	public void open(Session session, @PathParam("room") String room,
			@PathParam("nicname") String nicname){
		this.session = session;
		this.room = room;
		this.nicname = nicname;
		Rooms.getIstance().RegisterGrafikSoket(room, this);
	}
	
	/**
	 * Wird Aufgeruffen wenn eine Textnachricht eingeht
	 * @param session Session der Nachricht
	 * @param msg Die Nachricht an sich
	 */
	@OnMessage
	public void text(Session session, String msg){
		Gson gson = new Gson();
		Message message = gson.fromJson(msg, Message.class);
		switch (message.getTyp()) {
		case TextMassage:
			Rooms.getIstance().TextMessage(room, nicname, (ChatData)message.getData());
			break;
		case NewContent:
			Rooms.getIstance().AddHistoryEntry(room, (HistoryEntry)message.getData());
			break;
		case Close:
			Rooms.getIstance().UnRegisterGrafikSocket(room, this);
			break;
		case AllContent:
			sendHistory(Rooms.getIstance().GetHistory(room), true);
			break;
		case Filter:
			Rooms.getIstance().executeFilter(room, (String)message.getData());
			break;
		case Delete:
			Rooms.getIstance().delteHistoryEntry(room, (Integer) message.getData());
			break;
		default:
			break;
		}
	}
	
	/**
	 * Sendet eine Nachricht Asyncron damtit der Server nicht Blockiert wird
	 * @param msg Die Nachricht die Verschikt werden soll
	 */
	public void sendText(Message msg){
		Gson gson = new Gson();
		if(session.isOpen()){
			session.getAsyncRemote().sendText(gson.toJson(msg));
		}else{
			log.log(Level.WARNING, "session is closed");
		}
	}
	
	/**
	 * Sendet die History an den Clienten
	 * @param history Die History die Verschickt werden soll 
	 * @param reset True wenn die Gesamte Flache beim Clienten neu gezeichnet werden muss
	 */
	public void sendHistory(List<HistoryEntry> history, boolean reset){
		Gson gson = new Gson();
		Message msg = null;
		if(reset){
			msg = new Message(MessageType.Reset, history);
		}else{
			msg = new Message(MessageType.NewContent, history);
		}
		
		String Massage = gson.toJson(msg);
		
		if(session.isOpen()){
			session.getAsyncRemote().sendText(Massage);
		}else{
			log.log(Level.WARNING, "session is closed");
			Destroy();
		}
	}
	
	/**
	 * Wird Aufgeruffen wenn ein Fehler in der Verbindung aufgetreten ist
	 * wird der Socket entfernt und die Fehlermeldung gelockt
	 * @param session Session mit dem Fehler
	 * @param t Der Fehler
	 */
	@OnError
	public void error(Session session, Throwable t){
		log.log(Level.FINER, "Fehler bei der Connection", t);
		Destroy();		
	}
	
	@OnClose
	public void onClose(Session session, CloseReason closeReason){
		if(closing == false){
			Destroy();
		}
	}
	
	/**
	 * Methode zum Zerstoren des Kannals
	 */
	public void Destroy(){
		closing = true;
		Rooms.getIstance().UnRegisterGrafikSocket(room, this);
		this.nicname = null;
		try {
			if(session.isOpen()){
				session.close();
			}
		} catch (IOException e) {
			log.log(Level.FINER, "Session konnte nicht geschlossen werden", e);
		} catch (IllegalStateException e) {
			log.log(Level.FINER, "Session war bereits geschlossen", e);
		}
		
		this.session = null;
		this.room = null;
	}
}
