package com.australianfan.googlemail.controler;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.image.BufferedImage;

import com.australianfan.googlemail.data.HistoryEntry;
/**
 * Klasse Dient zum Zeichenen eines Images
 * @author Felix Bruns
 *
 */
public class DrawImage {
	/**
	 * Logger zum loggen von Fehlermeldungen
	 */
	private static final Logger log = Logger.getLogger(DrawImage.class.getName());

	/**
	 * Methode zum Zeichenen von einen Image
	 * @param history Liste mit History Eintraegen die gezeichnet werden sollen
	 * @return Das Fertige Image
	 */
	public static Image getImage(List<HistoryEntry> history) {
		//Erstellen eines Image zum Zeichen
		BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_3BYTE_BGR);
		Graphics2D g2 = image.createGraphics();
		//Damit die Linien gleich aussehen
		g2.setStroke(new BasicStroke(10, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));
		//Den Hintergrund Weiss machen
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, image.getWidth(), image.getHeight());
		
		//Fuer jeden Eintrag fueren wir eine Zeichenoperation aus.
		for (HistoryEntry entry : history) {
			//Java mag es nicht wenn man Kordinaten verdreht beim Rechtecke zeichnen.
			int x1 = 0, x2 = 0, y1 = 0, y2 = 0;
			if(entry.getPointX() < entry.getPointX2() || entry.getDrawType() == "line" || entry.getDrawType() == "cical"){
				x1 = entry.getPointX().intValue();
				x2 = entry.getPointX2().intValue();
			}else{
				x1 = entry.getPointX2().intValue();
				x2 = entry.getPointX().intValue();
			}
			if(entry.getPointY() < entry.getPointY2() || entry.getDrawType() == "line" || entry.getDrawType() == "cical"){
				y1 = entry.getPointY().intValue();
				y2 = entry.getPointY2().intValue();
			}else{
				y1 = entry.getPointY2().intValue();
				y2 = entry.getPointY().intValue();
			}
			
			//Ermitteln der Farbe in gezeichnet werden soll
			try {
				g2.setColor(Color.decode("0X" + entry.getColor().replace("#", "")));
			} catch (NumberFormatException ex) {
				log.log(Level.WARNING, "Farbe konnte nicht ermittelt werden", ex);
			}
			
			//Ermitteln was gezeichnet werden soll
			if (entry.getIsFill()) {
				switch (entry.getDrawType()) {
				case "line":
					g2.drawLine(entry.getPointX().intValue(), entry.getPointY().intValue(), entry.getPointX2().intValue(), entry.getPointY2().intValue());
					break;
				case "sqare":
					g2.fillRect(x1, y1, x2 - x1, y2 - y1);
					break;
				case "cical":
					int r = getRadius(entry.getPointX2() - entry.getPointX(),entry.getPointY2() - entry.getPointY());
					g2.fillArc(entry.getPointX().intValue() - r, entry.getPointY().intValue() - r, r * 2, r * 2, 0, 360);
					break;
				}
			} else {
				switch (entry.getDrawType()) {
				case "line":
					g2.drawLine(entry.getPointX().intValue(), entry.getPointY().intValue(), entry.getPointX2().intValue(), entry.getPointY2().intValue());
					break;
				case "sqare":
					g2.drawRect(x1, y1, x2 - x1, y2 - y1);
					break;
				case "cical":
					int r = getRadius(entry.getPointX2() - entry.getPointX(),entry.getPointY2() - entry.getPointY());
					g2.drawArc(entry.getPointX().intValue() - r, entry.getPointY().intValue() - r, r * 2, r * 2, 0, 360);
					break;
				}
			}
		}

		return image;
	}
	
	/**
	 * Metode zum errechnen des Radius
	 * @param x Laenge zum Seitenrand
	 * @param y Hohe
	 * @return Den Radius als int
	 */
	private static int getRadius(double x, double y){
		return (int)Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
}
