package com.australianfan.googlemail.controler;

import java.awt.Color;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import com.australianfan.googlemail.data.HistoryEntry;
import com.australianfan.googlemail.data.Rooms;
import com.sun.istack.internal.logging.Logger;
/**
 * Roobot der Zufallig was Zeichnet
 * @author Felix Bruns
 *
 */
public class Robot extends TimerTask{
	/**
	 * Konstruktor zum erstellen eines Robots
	 * @param room Raum in dem der Roboer Zeichen soll
	 */
	public Robot(String room){
		this.room = room;
	}	
	
	/**
	 * Logger zum loggen von Informationen
	 */
	private static Logger log = Logger.getLogger(Robot.class);
	
	/**
	 * Timer zum immer wieder zeichnen
	 */
	private Timer timer = null;
	
	/**
	 * Raum in dem gezeichnet werden soll
	 */
	private String room = null;
	
	/**
	 * Startet den Robot
	 */
	public void Start(){
		if(timer == null){
			timer = new Timer();
			timer.schedule(this, 5000, 5000);
			log.log(Level.INFO, "Robot fuer Raum \"" + room + "\" gestartet");
		}
	}
	
	/**
	 * Halt den Robot an
	 */
	public void Stop(){
		if(timer != null){
			timer.cancel();
			log.log(Level.INFO, "Robot fuer Raum \"" + room + "\" gestopt");
		}
		
		timer = null;
	}

	/**
	 * Erstelt eine Zufaligen HistoryEntry und fuegt in zum Raum hinzu
	 */
	@Override
	public void run() {
		HistoryEntry entry = new HistoryEntry();
		Color color = new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
		entry.setColor(Integer.toHexString(color.getRGB()).replace("ff", "#"));
		int drawType = (int)((Math.random() * 10) % 3);
		if(drawType == 0){
			entry.setDrawType("line");
		}else if(drawType == 1){
			entry.setDrawType("sqare");
		}else if(drawType == 2){
			entry.setDrawType("cical");
		}
		
		entry.setIsFill(Math.random() > 0.5);
		entry.setUser("Robot");
		entry.setPointX(Math.random() * 500);
		entry.setPointX2(entry.getPointX() + Math.random() * 100);
		entry.setPointY(Math.random() * 500);
		entry.setPointY2(entry.getPointY() + Math.random() * 100);
		
		Rooms.getIstance().AddHistoryEntry(room, entry);
	}
}
