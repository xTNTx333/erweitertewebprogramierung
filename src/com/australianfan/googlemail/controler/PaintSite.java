package com.australianfan.googlemail.controler;

import java.awt.Image;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.australianfan.googlemail.data.HistoryEntry;
import com.australianfan.googlemail.data.Rooms;
import com.australianfan.googlemail.view.SocketBean;
import com.sun.jersey.api.view.Viewable;
/**
 * Klasse zum Weiterleiten an die Richtigen Seiten
 * @author Felix Bruns
 *
 */
@Path("/paint")
public class PaintSite {

	/**
	 * Weiterleitung zur Seite wo mehere Nutzer gleichzeitig Zeichnen konnen
	 * @param request request des Clienten
	 * @param response response des Servers
	 * @param room Raum der betreten werden will
	 * @param nicname Name des Nutzers
	 * @return Seite zum Malen
	 */
	@GET
	@Path("multypaint/{room1}/{nickname1}")
	@Produces("text/html")
	public Viewable getPaintSite(@Context HttpServletRequest request,
			@Context HttpServletResponse response, @PathParam("room1") String room, @PathParam("nickname1") String nicname){
		if(room != null && !room.isEmpty() && nicname != null && !nicname.isEmpty()){
			SocketBean bean = new SocketBean(room, nicname);
			return new Viewable("/paint", bean);
		}
		
		return null;
	}
	
	/**
	 * Lifert ein einfaches Bild zuruck vom Channel
	 * @param channel Kannal vom dem das Bild erzeugt werden soll
	 * @return PNG
	 */
	@POST
	@Path("image")
	@Produces("image/png")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Image getImage(@FormParam("channel") String channel){
		return DrawImage.getImage(Rooms.getIstance().GetHistory(channel));
	}
	
	/**
	 * Liefert die History eines Raumes als JSON zurueck
	 * @param room Der Raum
	 * @return JSON String
	 */
	@GET
	@Path("json/{room}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<HistoryEntry> getJSon(@PathParam("room") String room){
		List<HistoryEntry> history = Rooms.getIstance().GetHistory(room);
		return history;
	}
}
